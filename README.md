# JUPYTER NOTEBOOK FOR DATA SCIENTIST

## Pre-requirements

1. Any IDE's like VS Code, Atom, Pycharm, etc.. (preferable VS Code)
2. Get Docker Desktop installed and running


## Setup
1. Navigate to the path where 'docker-composer.yml' exists and run below commands:
    >docker-compose -f docker-composer.yml up --build -d


2. Once docker got installed successfully. Click on the "Docker" icon in the VS Code. Check for "datascience:v1"

3. Right-Click on the image "datasciece:v1" and view logs. The logs will be shown in a new terminal with link simillar to beow:

http://127.0.0.1:9090/?token=7d0819c06bdb70f400919b2072b891903e336758735322df

4. Copy and paste the link in your browser. Replace 9090 (any port you mapped in docker-composer file) with 9080 to connect from your local machine.

It will open a new jupyter-notebook platform in the browser.

## About this Docker

The docker file is created keeping in mind the librarries used for data science and machine learning, this also support Spark dataframes.

